/* 1 */
{
    "_id" : "1",
    "libelle_fr" : "oeufs",
    "libelle_en" : "eggs",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 2 */
{
    "_id" : "2",
    "libelle_fr" : "gluten",
    "libelle_en" : "gluten",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 3 */
{
    "_id" : "3",
    "libelle_fr" : "crustac�s",
    "libelle_en" : "crustaceans",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 4 */
{
    "_id" : "4",
    "libelle_fr" : "poissons",
    "libelle_en" : "fish",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 5 */
{
    "_id" : "5",
    "libelle_fr" : "arachides",
    "libelle_en" : "peanuts",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 6 */
{
    "_id" : "6",
    "libelle_fr" : "soja",
    "libelle_en" : "soybeans",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 7 */
{
    "_id" : "7",
    "libelle_fr" : "lait",
    "libelle_en" : "milk",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 8 */
{
    "_id" : "8",
    "libelle_fr" : "fruits � coques",
    "libelle_en" : "nuts",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 9 */
{
    "_id" : "9",
    "libelle_fr" : "c�leri",
    "libelle_en" : "celery",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 10 */
{
    "_id" : "10",
    "libelle_fr" : "moutarde",
    "libelle_en" : "mustard",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 11 */
{
    "_id" : "11",
    "libelle_fr" : "graines de s�same",
    "libelle_en" : "sesame seeds",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 12 */
{
    "_id" : "12",
    "libelle_fr" : "anhydride sulfureux et sulfites",
    "libelle_en" : "sulphur dioxide and sulphites",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 13 */
{
    "_id" : "13",
    "libelle_fr" : "lupin",
    "libelle_en" : "lupin",
    "_class" : "com.example.demo.Model.Allergen"
}

/* 14 */
{
    "_id" : "14",
    "libelle_fr" : "mollusques",
    "libelle_en" : "molluscs",
    "_class" : "com.example.demo.Model.Allergen"
}