package rain.bot.com.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Document(collection = "Plante")
public class Plante {

    private ProfilPlante profil;

    @Id
    private String id;
    private String name;
    private float latitude;
    private float longitude;
    private float humidity;

    public Plante(String id, String name, ProfilPlante profil, float latitude, float longitude) {
        this.name = name;
        this.profil = profil;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Plante(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public ProfilPlante getProfil() {
        return profil;
    }

    public void setProfil(ProfilPlante profil) {
        this.profil = profil;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
