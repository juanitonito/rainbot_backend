package rain.bot.com.Model;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlanteRepository extends MongoRepository<Plante,String> {

}
