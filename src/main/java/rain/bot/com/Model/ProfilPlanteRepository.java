package rain.bot.com.Model;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProfilPlanteRepository extends MongoRepository<ProfilPlante,String> {

}
