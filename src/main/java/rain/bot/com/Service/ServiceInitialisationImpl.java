package rain.bot.com.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import rain.bot.com.Model.Plante;
import rain.bot.com.Model.PlanteRepository;
import rain.bot.com.Model.ProfilPlante;
import rain.bot.com.Model.ProfilPlanteRepository;

import java.util.List;
import java.util.Optional;

@Component
public class ServiceInitialisationImpl implements ServiceInitialisation{

    @Autowired
    ProfilPlanteRepository profilPlanteRepository;

    @Autowired
    PlanteRepository planteRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<ProfilPlante> showProfilsPlante() {
        return profilPlanteRepository.findAll();
    }

    @Override
    public Plante createPlante(Plante plante) {
        return planteRepository.insert(plante);
    }

    @Override
    public List<Plante> getPlantes() {
        return planteRepository.findAll();
    }

    @Override
    public Plante getDetailPlante(String planteId) {
        Optional<Plante> plante = planteRepository.findById(planteId);
        return plante.get();
    }
}
