package rain.bot.com.Service;

import rain.bot.com.Model.Plante;
import rain.bot.com.Model.ProfilPlante;

import java.util.List;
import java.util.Optional;

public interface ServiceInitialisation {
    List<ProfilPlante> showProfilsPlante();
    Plante createPlante(Plante plante);
    List<Plante> getPlantes();
    Plante getDetailPlante(String planteId);
}
