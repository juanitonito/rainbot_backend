package rain.bot.com.Controleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rain.bot.com.Model.Plante;
import rain.bot.com.Model.ProfilPlante;
import rain.bot.com.Service.ServiceInitialisation;

import java.util.List;


@RestController
public class InitilisationControleur {


    @Autowired
    private ServiceInitialisation service;


    @RequestMapping(value = "/plante/", method = RequestMethod.POST)
    public Plante createAllergen(@RequestBody Plante plante){
        return this.service.createPlante(plante);
    }

    @RequestMapping(value="/plantes/", method = RequestMethod.GET)
    public List<Plante> getPlantes() {
        return service.getPlantes();
    }

    @RequestMapping(value="/plante/{planteId}", method = RequestMethod.GET)
    public Plante getPlantes(@PathVariable("planteId") String planteId) {
        return service.getDetailPlante(planteId);
    }

    @RequestMapping(value="/profilsPlantes/", method = RequestMethod.GET)
    public List<ProfilPlante> showProfilsPlante() {
        return service.showProfilsPlante();
    }
}