package rain.bot.com.mqtt.subscriber;


import net.minidev.json.JSONObject;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Component;
import rain.bot.com.Service.ServiceInitialisation;
import rain.bot.com.config.MQTTConfig;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * MQTT Subscriber Class
 * 
 * @author Moniruzzaman Md
 *
 */

@Component
public class MQTTSubscriber extends MQTTConfig implements MqttCallback, MQTTSubscriberBase {
	private String brokerUrl = null;
	final private String colon = ":";
	final private String clientId = "demoClient2";

	private MqttClient mqttClient = null;
	private MqttConnectOptions connectionOptions = null;
	private MemoryPersistence persistence = null;
	private Boolean arrosageEnCours = false;

	private static final Logger logger = LoggerFactory.getLogger(MQTTSubscriber.class);

	public MQTTSubscriber() {
		this.config();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.
	 * Throwable)
	 */
	@Override
	public void connectionLost(Throwable cause) {
		logger.info("Connection Lost");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String,
	 * org.eclipse.paho.client.mqttv3.MqttMessage)
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		// Called when a message arrives from the server that matches any
		// subscription made by the client
		/*String time = new Timestamp(System.currentTimeMillis()).toString();
		System.out.println();
		System.out.println("***********************************************************************");
		System.out.println("Message Arrived at Time: " + time + "  Topic: " + topic + "  Message: "
				+ new String(message.getPayload()));
		System.out.println("***********************************************************************");
		System.out.println();*/
		Boolean arrosage = false;
		String json = new String(message.getPayload());
		JsonParser springParser = JsonParserFactory.getJsonParser();
		Map<String, Object> map = springParser.parseMap(json);
		String str_humi = String.valueOf(map.get("humidity"));
		Float humidity = Float.parseFloat(str_humi);
		if(humidity <= 60 && !arrosageEnCours){
				arrosageEnCours = true;
				//ENVOYER QUERY AU SERVEUR
				System.out.println("***********************************************************************");
				System.out.println(map.get("id") + ": " + map.get("humidity"));
				System.out.println("DEBUT ARROSAGE DE : " + map.get("id"));
				System.out.println("***********************************************************************");
				System.out.println();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho
	 * .client.mqttv3.IMqttDeliveryToken)
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// Leave it blank for subscriber

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.monirthought.mqtt.subscriber.MQTTSubscriberBase#subscribeMessage(java.
	 * lang.String)
	 */
	@Override
	public void subscribeMessage(String topic) {
		try {
			this.mqttClient.subscribe(topic, this.qos);
		} catch (MqttException me) {
			me.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monirthought.mqtt.subscriber.MQTTSubscriberBase#disconnect()
	 */
	public void disconnect() {
		try {
			this.mqttClient.disconnect();
		} catch (MqttException me) {
			logger.error("ERROR", me);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monirthought.config.MQTTConfig#config(java.lang.String,
	 * java.lang.Integer, java.lang.Boolean, java.lang.Boolean)
	 */
	@Override
	protected void config(String broker, Integer port, Boolean ssl, Boolean withUserNamePass) {

		String protocal = this.TCP;
		if (true == ssl) {
			protocal = this.SSL;
		}

		this.brokerUrl = protocal + this.broker + colon + port;
		this.persistence = new MemoryPersistence();
		this.connectionOptions = new MqttConnectOptions();

		try {
			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence);
			this.connectionOptions.setCleanSession(true);
			if (true == withUserNamePass) {
				if (password != null) {
					this.connectionOptions.setPassword(this.password.toCharArray());
				}
				if (userName != null) {
					this.connectionOptions.setUserName(this.userName);
				}
			}
			this.mqttClient.connect(this.connectionOptions);
			this.mqttClient.setCallback(this);
		} catch (MqttException me) {
			me.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monirthought.config.MQTTConfig#config()
	 */
	@Override
	protected void config() {

		this.brokerUrl = this.TCP + this.broker + colon + this.port;
		this.persistence = new MemoryPersistence();
		this.connectionOptions = new MqttConnectOptions();
		try {
			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence);
			this.connectionOptions.setCleanSession(true);
			this.mqttClient.connect(this.connectionOptions);
			this.mqttClient.setCallback(this);
		} catch (MqttException me) {
			me.printStackTrace();
		}

	}

}
