package rain.bot.com.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rain.bot.com.mqtt.subscriber.MQTTSubscriberBase;

@Component
public class MessageListener implements Runnable{

	@Autowired
	MQTTSubscriberBase subscriber;
	
	@Override
	public void run() {
		while(true) {
			subscriber.subscribeMessage("sensor/#");
		}
		
	}

}
