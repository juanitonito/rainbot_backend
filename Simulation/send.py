#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import time,sys, json

#python ./send.py 192.168.1.64 sensor/54343453
#python ./send.py 192.168.1.64 sensor/6987845

# This is the Publisher

client = mqtt.Client()
client.connect(sys.argv[1],1883,60)

for i in range(100,1,-1):
    # a Python object (dict):
    data = {
        "id": sys.argv[2],
        "humidity": i
    }

    client.publish(sys.argv[2], json.dumps(data))
    time.sleep(1)

client.disconnect();